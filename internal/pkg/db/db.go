package db

import (
	"context"
	"database/sql"
)

//go:generate mockgen -source=db.go -destination mocks_db/mock.go -package mocks_db
type Executor interface {
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	GetContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error
	SelectContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error
}
