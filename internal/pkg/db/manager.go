package db

import (
	"context"
	"database/sql"
	"sync"

	"github.com/jmoiron/sqlx"

	"gitlab.com/project54/example/internal/pkg/db/transaction"
)

type Manager struct {
	db *sqlx.DB

	mu           *sync.RWMutex
	transactions map[transaction.ID]*sqlx.Tx
}

func (m *Manager) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	tx, ok := m.getTx(ctx)
	if !ok {
		return m.db.ExecContext(ctx, query, args...)
	}
	return tx.ExecContext(ctx, query, args...)
}

func (m *Manager) GetContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	tx, ok := m.getTx(ctx)
	if !ok {
		return m.db.GetContext(ctx, dest, query, args...)
	}
	return tx.GetContext(ctx, dest, query, args...)
}

func (m *Manager) SelectContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	tx, ok := m.getTx(ctx)
	if !ok {
		return m.db.SelectContext(ctx, dest, query, args...)
	}
	return tx.SelectContext(ctx, dest, query, args...)
}

func (m *Manager) getTx(ctx context.Context) (*sqlx.Tx, bool) {
	id, ok := transaction.GetTransactionID(ctx)
	if !ok {
		return nil, false
	}

	m.mu.RLock()
	defer m.mu.RUnlock()

	tx, ok := m.transactions[id]
	if !ok {
		return nil, false
	}
	return tx, true
}
