// Code generated by MockGen. DO NOT EDIT.
// Source: transactor.go

// Package mocks_transaction is a generated GoMock package.
package mocks_transaction

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	transaction "gitlab.com/project54/example/internal/pkg/db/transaction"
)

// MockTransactor is a mock of Transactor interface.
type MockTransactor struct {
	ctrl     *gomock.Controller
	recorder *MockTransactorMockRecorder
}

// MockTransactorMockRecorder is the mock recorder for MockTransactor.
type MockTransactorMockRecorder struct {
	mock *MockTransactor
}

// NewMockTransactor creates a new mock instance.
func NewMockTransactor(ctrl *gomock.Controller) *MockTransactor {
	mock := &MockTransactor{ctrl: ctrl}
	mock.recorder = &MockTransactorMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockTransactor) EXPECT() *MockTransactorMockRecorder {
	return m.recorder
}

// InTransaction mocks base method.
func (m *MockTransactor) InTransaction(arg0 context.Context, arg1 func(transaction.Context) error) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "InTransaction", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// InTransaction indicates an expected call of InTransaction.
func (mr *MockTransactorMockRecorder) InTransaction(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InTransaction", reflect.TypeOf((*MockTransactor)(nil).InTransaction), arg0, arg1)
}
