package transaction

import (
	"context"
)

//go:generate mockgen -source=$GOFILE -destination mocks_$GOPACKAGE/mock_$GOFILE -package mocks_$GOPACKAGE
type Context interface {
	context.Context
	GetTransactionID() (ID, bool)
}

type key int

var txKey key

func ContextWithTx(ctx context.Context, id ID) Context {
	return &txContext{context.WithValue(ctx, txKey, id)}
}

func GetTransactionID(ctx context.Context) (ID, bool) {
	id, ok := ctx.Value(txKey).(ID)
	return id, ok
}

type txContext struct {
	context.Context
}

func (t *txContext) GetTransactionID() (ID, bool) {
	return GetTransactionID(t.Context)
}
