package transaction

import (
	"context"

	"github.com/google/uuid"
)

type ID uuid.UUID

//go:generate mockgen -source=$GOFILE -destination mocks_$GOPACKAGE/mock_$GOFILE -package mocks_$GOPACKAGE
type Manager interface {
	Begin(ctx context.Context) (ID, error)
	Rollback(ID) error
	Commit(ID) error
}
