package transaction

import "context"

//go:generate mockgen -source=$GOFILE -destination mocks_$GOPACKAGE/mock_$GOFILE -package mocks_$GOPACKAGE
type Transactor interface {
	InTransaction(context.Context, func(txCtx Context) error) error
}
