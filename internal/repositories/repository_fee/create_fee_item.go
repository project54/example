package repository_fee

import (
	"context"

	"gitlab.com/project54/example/internal/models"
	"gitlab.com/project54/example/internal/pkg/db/transaction"
)

func (r *FeeDbRepository) CreateFeeItem(ctx transaction.Context, id int) (fee models.FeeItem, err error) {
	_, err = r.db.ExecContext(ctx, "addfa")
	if err != nil {
		return models.FeeItem{}, err
	}
	r.someAction(ctx)
	return models.FeeItem{}, nil
}

func (r *FeeDbRepository) someAction(ctx context.Context) {
	return
}
