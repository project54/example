package repository_fee

import (
	"gitlab.com/project54/example/internal/models"
	"gitlab.com/project54/example/internal/pkg/db"
	"gitlab.com/project54/example/internal/pkg/db/transaction"
)

//go:generate mockgen -source=$GOFILE -destination mocks_$GOPACKAGE/mock_$GOFILE -package mocks_$GOPACKAGE
type Repository interface {
	CreateFeeItem(ctx transaction.Context, id int) (fee models.FeeItem, err error)
}

type FeeDbRepository struct {
	db db.Executor
}
