package service_create_fee

import (
	"context"

	"gitlab.com/project54/example/internal/models"
	"gitlab.com/project54/example/internal/pkg/db/transaction"
	"gitlab.com/project54/example/internal/repositories/repository_fee"
	"gitlab.com/project54/example/internal/services/service_waiting"
)

type Service interface {
	CreateFee(ctx context.Context, id int) (models.FeeItem, error)
}

type CreateFeeService struct {
	repo           repository_fee.Repository
	transactor     transaction.Transactor
	waitingService service_waiting.WaitingService
}

func (s *CreateFeeService) CreateFee(ctx context.Context, id int) (fee models.FeeItem, err error) {
	err = s.transactor.InTransaction(ctx, func(txCtx transaction.Context) (err error) {
		fee, err = s.createFee(txCtx, id)
		if err != nil {
			return err
		}
		return nil
	})
	return
}

func (s *CreateFeeService) createFee(tx transaction.Context, id int) (fee models.FeeItem, err error) {
	fee, err = s.repo.CreateFeeItem(tx, id)
	if err != nil {
		return
	}
	err = s.waitingService.SetWaiting(tx, false)
	if err != nil {
		return
	}
	return fee, nil
}
