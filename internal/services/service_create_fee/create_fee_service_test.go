package service_create_fee

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"

	"gitlab.com/project54/example/internal/models"
	"gitlab.com/project54/example/internal/pkg/db/transaction"
	"gitlab.com/project54/example/internal/pkg/db/transaction/mocks_transaction"
	"gitlab.com/project54/example/internal/repositories/repository_fee/mocks_repository_fee"
	"gitlab.com/project54/example/internal/services/service_waiting/mocks_service_waiting"
)

func TestCreateFeeSrvice_Create(t *testing.T) {

	ctrl := gomock.NewController(t)

	txContextMock := mocks_transaction.NewMockContext(ctrl)

	transactorMock := mocks_transaction.NewMockTransactor(ctrl)
	transactorMock.
		EXPECT().
		InTransaction(gomock.Any(), gomock.AssignableToTypeOf(*new(func(t transaction.Context) error))).
		DoAndReturn(func(ctx context.Context, f func(t transaction.Context) error) error {
			return f(txContextMock)
		})

	mockRepo := mocks_repository_fee.NewMockRepository(ctrl)

	mockRepo.EXPECT().CreateFeeItem(txContextMock, 1).Return(models.FeeItem{}, nil)

	mockWaitingService := mocks_service_waiting.NewMockWaitingService(ctrl)
	mockWaitingService.EXPECT().SetWaiting(txContextMock, false).Return(nil)

	service := &CreateFeeService{
		repo:           mockRepo,
		transactor:     transactorMock,
		waitingService: mockWaitingService,
	}

	_, err := service.CreateFee(context.Background(), 1)
	require.NoError(t, err)
}
