package service_waiting

import (
	"gitlab.com/project54/example/internal/pkg/db/transaction"
)

//go:generate mockgen -source=$GOFILE -destination mocks_$GOPACKAGE/mock_$GOFILE -package mocks_$GOPACKAGE
type WaitingService interface {
	SetWaiting(txCtx transaction.Context, waiting bool) error
}
